import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../environments/environment';
import {Observable} from 'rxjs';

@Injectable()
export class ApiService {
  apiUrl: string;
  headers: any;
  result:any;
  constructor(private http: HttpClient) {
    this.apiUrl = environment.apiUrl;
  }

  /**
   * Cikkek
   */
  getPosts() {
    return this.http.get(this.apiUrl + 'posts');
  }

  /**
   * Cikk commentek
   * @param id
   */
  getPostComments(id) {
    return this.http.get(this.apiUrl + 'posts/' + id + '/comments');
  }

  /**
   * Post küldése
   * @param title
   * @param body
   */
  sendPost(title: string, body: string): Observable<any> {
    return this.http.post<any>(this.apiUrl + 'posts', {title: title, body: body});
  }
}
