import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {PageNotFoundComponent} from './page-not-found/page-not-found.component';
import {CommentsComponent} from './comments/comments.component';
import {PostsComponent} from './posts/posts.component';

const appRoutes: Routes = [
  {path: 'posts', component: PostsComponent},
  {path: 'comments/:id', component: CommentsComponent},
  {path: '', redirectTo: '/posts', pathMatch: 'full'},
  {path: '**', component: PageNotFoundComponent}
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes, { useHash: true }),
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {
}
