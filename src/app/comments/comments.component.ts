import {Component, OnInit} from '@angular/core';
import {ApiService} from '../api.service';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import {Location} from '@angular/common';

@Component({
  selector: 'app-comments',
  templateUrl: './comments.component.html',
  styleUrls: ['./comments.component.css']
})
export class CommentsComponent implements OnInit {
  id: number;
  comments: any;
  private sub: any;

  constructor(private api: ApiService, private route: ActivatedRoute,private location: Location) {}

  ngOnInit(): void {
    /* Paraméter lekérése, beállítása */
    this.sub = this.route.params.subscribe(params => {
      this.id = params.id;
    });
    this.getPostComments();
  }

  /**
   * Kommentek megjelenítése
   */
  getPostComments(){
    this.api.getPostComments(this.id)
      .toPromise()
      .then((res) => {
        this.comments = res;
      })
      .catch(err => {
        console.log(err);
      });
  }
}

