import {Component, OnInit} from '@angular/core';
import {ApiService} from '../api.service';
import {FormBuilder, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {
  posts: any;
  postCreate: boolean = false;
  form: FormGroup;
  postResult: any;
  postSendLoader: boolean = false;
  showError: boolean = false;
  showSuccess: boolean = false;
  postOkay: boolean = true;

  constructor(private api: ApiService, private fb: FormBuilder) {
    this.form = this.fb.group({
      title: [null],
      body: [null]
    });
  }

  /**
   * Cikkek lekérése
   */
  ngOnInit(): void {
    this.getPosts();
  }

  /**
   * Postok lekérése
   */
  getPosts() {
    this.api.getPosts()
      .toPromise()
      .then((res) => {
        this.posts = res;
      })
      .catch(err => {
        console.log(err);
      });
  }

  /**
   * Cikk form toggle
   * @param show
   */
  showPostForm(show) {
    this.postSendLoader = false;
    this.postCreate = show;
    this.showError = false;
    this.showSuccess = false;
  }

  /**
   * Adatok ellenőrzése
   * @param str
   */
  checkEmpty(str){
     return (!str || 0 === str.length);
  }

  /**
   * Cikkek beküldése
   */
  sendPost() {

    this.postSendLoader = true;
    this.showError = false;
    this.showSuccess = false;

    const title = this.form.get('title').value;
    const body = this.form.get('body').value;

    if (this.checkEmpty(title) || this.checkEmpty(body)) {
      this.showError = true;
      this.postSendLoader = false;
      this.postOkay = false;
    }
    if (this.postOkay === true) {
      this.api.sendPost(title, body)
        .toPromise()
        .then((res) => {
          if (res.title) {
            this.showSuccess = true;
            this.postSendLoader = false;
          } else {
            this.showError = false;
            this.postSendLoader = false;
          }
        })
        .catch(err => {
          this.showError = err;
          this.postSendLoader = false;
        });
    }
  }

}
